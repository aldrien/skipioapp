$(document).ready(function(){
	var $btnTrigger = $('button.trigger_modal');
	var $sendMessageModal = $('div#sendMessageModal');
	var $btnSend = $('button#btn_send_message');
	var $message = $('textarea#message');
	var $recipient_name = $('#recipient_name');
	var $notification = $('p#notification');

	var reset_fields =function(){
		$notification.removeClass().text('');
		$message.val('').focus();
	}

	// Send Message Button in List
	$btnTrigger.on('click', function(){
		reset_fields();

		// Show recipient name
		$recipient_name.text($(this).data('name'));

		// Set up modal
		$sendMessageModal.modal({
		  backdrop: 'static',
		  keyboard: false
		});
	});

	// Send Message
	$btnSend.on('click', function(){
		if ($message.val().length < 1){
			$notification.addClass('alert alert-danger').text('Please enter your message!');
			$message.focus()
			return
		}

		$(this).prop('disabled', true).text('SENDING...');

		$.ajax({
			type: 'POST',
			url: '/users/send_message',
			data: {
			    message: $message.val(),
			    recipient: $btnTrigger.data('id')
			}
		})
		.fail(function(data){
			console.log("Error: " + data)
			$notification.addClass('alert alert-danger').text('Sorry, there was an error sending your message.');
			$btnSend.prop('disabled', false).text('SEND');
		})
		.done(function(data){
			$notification.removeClass();

			if (data.status == 'success') {
				set_class = "alert-success";
			} else {
				set_class = "alert-danger";
			}
			$notification.addClass('alert ' + set_class).text(data.message);

			console.log(data);

			$message.val('').focus();
			$btnSend.prop('disabled', false).text('SEND');
		});

		
	});
});