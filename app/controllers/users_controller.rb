class UsersController < ApplicationController
  require 'will_paginate/array'
  require 'rest-client'

  @@skipio_token = Figaro.env.skipio_access_token

  def index
    response = RestClient.get 'https://stage.skipio.com/api/v2/contacts', {
      accept: :json,
      params: {
        token: @@skipio_token
      }
    }
    @total_contacts = JSON.parse(response.body)['meta']['total_count']
    @contact_lists = JSON.parse(response.body)['data'].paginate(:page => params[:page], :per_page => 5)
  end

  def send_message
    begin
      response = RestClient.post 'https://stage.skipio.com/api/v2/messages', 
        body: {
          token: @@skipio_token,
          recipients: {
            '' => ["contact-#{params[:recipient]}"]
          },
          message: {
            body: params[:message]
          }
        }

      response = JSON.parse(response.body)

      response = HTTParty.post('https://stage.skipio.com/api/v2/messages',
        body:{
          token: @@skipio_token,
          recipients: ["contact-#{params[:recipient]}"],
          message: {
            body: params[:message]
          }
        }
      )
      # response = HTTParty.post("https://stage.skipio.com/api/v2/messages?token=#{@@skipio_token}?recipients['contact-#{params[:recepient]}']&message['body']=#{params[:message]}")
    rescue => e
      response = e
    end

  #   TO BE CONTINUED......
  #   if response['errors'].length > 0
  # 		error_msg = "Error: "
  # 		response['errors'].each do |e|
  # 			error_msg +=  "#{e}. "
		# 	end
  # 		render json: {status: :error, message: error_msg}
  # 	else
  # 		render json: {status: :success, message: 'Message was successfully sent.'}
		# end
    render json: response
  end
end