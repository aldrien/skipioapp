Rails.application.routes.draw do
  resources :users, only: [:index] do
  	collection do
  		post 'send_message'
		end
  end
  
  root :to => 'users#index' 
end
