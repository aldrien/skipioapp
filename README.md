# README

This README would normally document whatever steps are necessary to get the
application up and running.

* Configuration
	** Set Environment Variable for Skipio Token
			> create file inside skipio_app directory "app/config/appication.yml"
			> add below code: 
				skipio_access_token: 'your_token_here'

* How to run the test suite
	** Run RSPEC All Test
			> rspec spec/features/*

	** Run RSPEC Specific Test
			> rspec spec/features/spec-name-here.rb 

* How to run Application
	** After cloning the repository:
			> open Terminal
			> cd app_directory
			> bundle install
			> rails server or rails s
	** Open in browser:
			> http://localhost:3000 or http://127.0.0.1:3000
