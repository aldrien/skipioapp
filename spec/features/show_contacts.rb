require "rails_helper"

# ALIASES

# feature == describe
# :type => :feature
# background == before
# scenario == it
# given/given! == let/let!

# https://github.com/jnicklas/capybara/blob/master/lib/capybara/node/actions.rb#L106

RSpec.feature "Show User Contacts", :type => :feature do  
  background do
    @skipio_token = Figaro.env.skipio_access_token

    response = RestClient.get 'https://stage.skipio.com/api/v2/contacts', {
      accept: :json,
      params: {
        token: @skipio_token
      }
    }
    @response = JSON.parse(response.body)['data'][0]
  end

  scenario "go to user show contacts page" do    
    visit users_path

    expect(page).to have_text('Listing Contacts')
    expect(page).to have_text(@response['full_name'])
    expect(page).to have_text(@response['phone_mobile'])
    expect(page).to have_text(@response['phone_mobile_national'])
    expect(page).to have_text(@response['phone_mobile_international'])
    expect(page).to have_text(@response['email'])
  end
end