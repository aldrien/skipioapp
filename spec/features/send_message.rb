require "rails_helper"

RSpec.feature "Send Message To Contact", :type => :feature, :js => true do
  background do
    @skipio_token = Figaro.env.skipio_access_token
    response = RestClient.get 'https://stage.skipio.com/api/v2/contacts', {
      accept: :json,
      params: {
        token: @skipio_token
      }
    }
  end

  scenario "send new message" do    
    visit users_path

    expect(page).to have_text('Listing Contacts')
    find(:xpath, "//table[@id='contacts_table']/tbody/tr[1]/td[8]/button[text()='Send Message']").click
    expect(page).to have_text('Enter your message here:')
    fill_in "message", with: "Sample message"
    click_button 'SEND'
    expect(page).to have_text('Message was successfully sent.')
  end

  scenario "validate blank message" do    
    visit users_path

    expect(page).to have_text('Listing Contacts')
    find(:xpath, "//table[@id='contacts_table']/tbody/tr[1]/td[8]/button[text()='Send Message']").click
    expect(page).to have_text('Enter your message here:')
    click_button 'SEND'
    expect(page).to have_text('Please enter your message!')
    expect(page).to have_css '.alert-danger', 'Please enter your message!'
  end
end